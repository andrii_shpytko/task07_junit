package com.epam.view;

import com.epam.controller.MinesweeperController;

import java.util.LinkedHashMap;
import java.util.Map;

public class MinesweeperMenu implements Menu {
    private MinesweeperController minesweeperController;

    public MinesweeperMenu(MinesweeperController minesweeperController) {
        this.minesweeperController = minesweeperController;
    }


    @Override
    public String initMenu() {
        return "Minesweeper";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<String, String>() {
            {
                put("1", "1\tCreate new board");
                put("2", "2\tPrint board [- solution]");
                put("3", "3\tPrint board [+ solution]");
                put("Q", "Q\texit");
            }
        };
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<String, Runnable>() {
            {
                put("1", () -> minesweeperController.createBoard(
                        View.inputInteger("m(x)"),
                        View.inputInteger("n(y)"),
                        View.inputDouble("p(%)")
                ));
                put("2", () -> minesweeperController.printBoardWithoutSolution());
                put("3", () -> minesweeperController.printBoardWithSolution());
                put("Q", () -> System.out.println("\nBack to the main menu"));
            }
        };
    }
}
