package com.epam.view;

import com.epam.controller.LongestPlateauController;

import java.util.LinkedHashMap;
import java.util.Map;

public class LongestPlateauMenu implements Menu {
    private LongestPlateauController longestPlateauController;

    public LongestPlateauMenu(LongestPlateauController longestPlateauController) {
        this.longestPlateauController = longestPlateauController;
    }

    @Override
    public String initMenu() {
        return "Longest plateau";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<String, String>() {
            {
                put("1", "1\tPrint array");
                put("2", "2\tSet array random");
                put("3", "3\tSet array custom");
                put("4", "4\tPrint sequence");
                put("Q", "Q\tExit");
            }
        };
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<String, Runnable>(){{
            put("1", () -> longestPlateauController.printArray());
            put("2", () -> longestPlateauController.setArrayRandom(
                    View.inputInteger("size"),
                    View.inputInteger("min value"),
                    View.inputInteger("max value")
            ));
            put("3", () -> longestPlateauController.setArrayCustom(View.inputIntegers()));
            put("4", () -> longestPlateauController.printLongestCountiguousSequense());
            put("Q", () -> System.out.println("\nback to the main menu"));
        }};
    }
}
