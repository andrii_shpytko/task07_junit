package com.epam.view;

import com.epam.controller.LongestPlateauController;
import com.epam.controller.MinesweeperController;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {
    private LongestPlateauController longestPlateauController;
    private MinesweeperController minesweeperController;

    public MainMenu() {
        longestPlateauController = new LongestPlateauController();
        minesweeperController = new MinesweeperController();
    }

    @Override
    public String initMenu() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<String, String>() {
            {
                put("1", "1\tLongest Plateau");
                put("2", "2\tMinesweeper");
                put("Q", "Q\texit");
            }
        };
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<String, Runnable>() {
            {
                put("1", () -> new LongestPlateauMenu(longestPlateauController).launch());
                put("2", () -> new MinesweeperMenu(minesweeperController).launch());
                put("Q", () -> System.out.println("\nBuy!"));
            }
        };
    }
}
