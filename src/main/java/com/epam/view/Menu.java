package com.epam.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public interface Menu {
    String initMenu();

    Map<String, String> initDisplay();

    Map<String, Runnable> initExecution();

    default void launch() {
        String menuName = initMenu();
        Map<String, String> items = initDisplay();
        Map<String, Runnable> menu = initExecution();
        startMenu(menuName, items, menu);

    }

    default void startMenu(String menuName, Map<String, String> items, Map<String, Runnable> menu) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String keyMenu = null;
        do {
            System.out.println("\n" + menuName);
            items.values().forEach((String item) -> System.out.println(" " + item));
            System.out.print("\nSelect menu item: ");
            try {
                keyMenu = br.readLine().toUpperCase();
                menu.get(keyMenu).run();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                System.out.println("\ninvalid input; please re-enter");
            }
        } while (!"Q".equals(keyMenu));
    }
}
