package com.epam.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.IntStream;

public class View {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    static int[] inputIntegers() {
        int index = 0;
        IntStream.Builder integerBuilder = IntStream.builder();
        System.out.println("<< enter empty line for stopping input numbers >>");
        while (true) {
            System.out.print("[" + index + "]");
            try {
                String userInput  = br.readLine();
                if(!userInput.isEmpty()){
                    integerBuilder.add(Integer.parseInt(br.readLine()));
                    index++;
                } else{
                    break;
                }
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
                System.out.println("Invalid input. Please re-enter");
            }
        }
        return integerBuilder.build().toArray();
    }

    static int inputInteger(String line){
        while(true){
            System.out.print(line + ": ");
            try {
                return Integer.parseInt(br.readLine());
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
                System.out.println("Invalid input. Please re-enter");
            }
        }
    }

    static double inputDouble(String line){
        while (true){
            System.out.print(line + ": ");
            try {
                return Double.parseDouble(br.readLine());
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
                System.out.println("Invalid input. Please re-enter");
            }
        }
    }
}
