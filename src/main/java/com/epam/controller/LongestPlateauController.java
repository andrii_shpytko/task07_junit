package com.epam.controller;

import com.epam.model.LongestPlateau;
import com.epam.model.NumberGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Map;
import java.util.Random;

public class LongestPlateauController {
    private static Logger logger = LogManager.getLogger(LongestPlateauController.class);

    private LongestPlateau longestPlateau;
    private NumberGenerator numberGenerator;

    public LongestPlateauController() {
        longestPlateau = new LongestPlateau();
        numberGenerator = (size, min, max) -> new Random().ints(size, min, max).toArray();
    }

    public void printArray() {
        int[] numbers = longestPlateau.getNumbers();
        logger.info("received array " + Arrays.toString(numbers));
    }

    public void setArrayRandom(int size, int minValue, int maxValue) {
        setArrayCustom(numberGenerator.generate(size, minValue, maxValue));
    }

    public void setArrayCustom (int... numbers){
        longestPlateau.setNumbers(numbers);
        logger.info("array has been updated " + Arrays.toString(numbers));
    }

    public void printLongestCountiguousSequense(){
        Map<Integer, Integer> sequence = longestPlateau.getLongestContiguousSequence();
        logger.info("received longest contiguous sequence; length = " + sequence.size() + ", " + sequence);
    }


}
