package com.epam.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class LongestPlateau {
    private int[] numbers;

    public LongestPlateau() {
        numbers = null;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }

    public Map<Integer, Integer> getLongestContiguousSequence() {
        if (numbers == null) {
            throw new IllegalArgumentException("array doesn't exist");
        }
        if (numbers.length == 0) {
            throw new IllegalArgumentException("array shouldn't be empty");
        }
        Map<Integer, Integer> longestSequence = new LinkedHashMap<>();
        for (int i = 0, j = 0; i < numbers.length;) {
            if ((i = skipToBigger(i)) == -1) {
                break;
            }
            if ((j = skipToIdentities(i)) == -1) {
                break;
            }
            if (isSequenceValid(i - 1, j)) {
                Map<Integer, Integer> sequence = getSequence(i - 1, j);
                if (sequence.size() > longestSequence.size()) {
                    longestSequence = sequence;
                }
            }
        }
        return longestSequence;
    }

    private int skipToIdentities(int fromIndex) {
        for(int i = fromIndex + 1; i < numbers.length; i++){
            if(numbers[fromIndex] != numbers[i]){
                return i;
            }
        }
        return -1;
    }

    private Map<Integer, Integer> getSequence(int fromIndex, int toIndex) {
        Map<Integer, Integer> sequence = new LinkedHashMap<>();
        for (int i = fromIndex; i <= toIndex; i++) {
            sequence.put(i, numbers[i]);
        }
        return sequence;
    }

    private boolean isSequenceValid(int fromIndex, int toIndex) {
        return numbers[fromIndex] < numbers[fromIndex + 1] && numbers[toIndex] < numbers[toIndex - 1];
    }

    private int skipToBigger(int fromIndex) {
        for (int i = fromIndex + 1; i < numbers.length; i++) {
            if (numbers[i] > numbers[i - 1]) {
                return i;
            }
        }
        return -1;
    }
}