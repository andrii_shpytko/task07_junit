package com.epam.model;

import org.joda.time.IllegalInstantException;

import java.util.Random;

public class Minesweeper {
    private int m;
    private int n;
    private boolean[][] board;

    public Minesweeper() {
        m = 0;
        n = 0;
        board = new boolean[m][n];
    }

    public boolean[][] getBoard() {
        return board;
    }

    public void createBoard(int m, int n, double percentage) {
        if (m <= 0 || n <= 0) {
            throw new IllegalArgumentException("M and N must be positive; m = " + m + ", n = " + n);
        }
        if (percentage < 0 || percentage > 100) {
            throw new IllegalInstantException("percentage must be in range [0 - 100]; p= " + percentage);
        }
        this.m = m;
        this.n = n;
        board = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] = occupiedWithProbability(percentage);
            }
        }
    }

    private boolean occupiedWithProbability(double percentage) {
        return new Random().nextDouble() * 100 < percentage;
    }

    public char[][] getBoardWithSolution() {
        char[][] boardWithSolution = new char[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                boardWithSolution[i][j] = board[i][j] ? '*' : getNumberNeighboringBombs(i, j);
            }
        }
        return boardWithSolution;
    }

    private char getNumberNeighboringBombs(int x, int y) {
        int bombs = 0;
        if (cellHasBomb(x - 1, y - 1)) {
            bombs++;
        }
        if (cellHasBomb(x - 1, y)) {
            bombs++;
        }
        if (cellHasBomb(x - 1, y + 1)) {
            bombs++;
        }
        if (cellHasBomb(x, y - 1)) {
            bombs++;
        }
        if (cellHasBomb(x, y + 1)) {
            bombs++;
        }
        if (cellHasBomb(x + 1, y - 1)) {
            bombs++;
        }
        if (cellHasBomb(x + 1, y)) {
            bombs++;
        }
        if (cellHasBomb(x + 1, y + 1)) {
            bombs++;
        }
        return (char) (bombs + '0');
    }

    private boolean cellHasBomb(int x, int y) {
        return cellExists(x, y) && board[x][y];
    }

    private boolean cellExists(int x, int y) {
        return (x >= 0 && x < m) && y >= 0 && y < n;
    }

    public char[][] getBoardWithoutSolution() {
        char[][] boardWithoutSolution = new char[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                boardWithoutSolution[i][j] = board[i][j] ? '*' : '.';
            }
        }
        return boardWithoutSolution;
    }
}
