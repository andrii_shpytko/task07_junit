package com.epam.model;

public interface NumberGenerator {
    int[] generate(int limit, int low, int hight);
}
