package com.epam.controller;

import com.epam.model.LongestPlateau;
import com.epam.model.NumberGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class LongestPlateauControllerTest {

    @InjectMocks
    private LongestPlateauController longestPlateauController;
    @Mock
    private LongestPlateau longestPlateau;
    @Mock
    private NumberGenerator numberGenerator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void printArrayInvokesGetNumbersTest() {
        longestPlateauController.printArray();
        verify(longestPlateau, times(1)).getNumbers();
    }

    @Test
    void setArrayRandomInvokesAndSetRandomCustomGenerateTest() {
        longestPlateauController.setArrayRandom(anyInt(), anyInt(), anyInt());
        verify(numberGenerator, times(1)).generate(anyInt(), anyInt(), anyInt());
        verify(longestPlateau, times(1)).setNumbers(any());
    }

    @Test
    void setArrayCustomInvokesSetArrayTest() {  // setArrayCustom_invokes_setArray_test
        longestPlateauController.setArrayCustom(any());
        verify(longestPlateau, times(1)).setNumbers(any());
    }

    @Test
    void printLongestContiguousSequenceTest() {
        longestPlateauController.printLongestCountiguousSequense();
        verify(longestPlateau, times(1)).getLongestContiguousSequence();
    }
}