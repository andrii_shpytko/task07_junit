package com.epam.controller;

import com.epam.model.Minesweeper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MinesweeperControllerTest {
    @InjectMocks
    private MinesweeperController minesweeperController;
    @Mock
    private Minesweeper minesweeper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createBoardTest() {
        minesweeperController.createBoard(anyInt(), anyInt(), anyDouble());
        verify(minesweeper, times(1)).createBoard(anyInt(), anyInt(), anyDouble());
    }

    @Test
    void printBoardWithSolutionTest() {
        when(minesweeper.getBoardWithSolution()).thenReturn(new char[0][0]);
        minesweeperController.printBoardWithSolution();
        verify(minesweeper, times(1)).getBoardWithSolution();
    }

    @Test
    void printBoardWithoutSolutionTest() {
        when(minesweeper.getBoardWithoutSolution()).thenReturn(new char[0][0]);
        minesweeperController.printBoardWithoutSolution();
        verify(minesweeper, times(1)).getBoardWithoutSolution();
    }
}