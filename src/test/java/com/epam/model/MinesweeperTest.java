package com.epam.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MinesweeperTest {

    @Test
    void createBoardWithInvalid_m_ArgumentTest() {
        Minesweeper minesweeper = new Minesweeper();
        assertThrows(IllegalArgumentException.class,
                () -> minesweeper.createBoard(-100, 50, 25));
    }

    @Test
    void createBoardWithInvalid_n_ArgumentTest() {
        Minesweeper minesweeper = new Minesweeper();
        assertThrows(IllegalArgumentException.class,
                () -> minesweeper.createBoard(10, -50, 45));
    }

    @Test
    void createBoardWithInvalid_percentage_ArgumentTest() {
        Minesweeper minesweeper = new Minesweeper();
        assertThrows(IllegalArgumentException.class,
                () -> minesweeper.createBoard(20, 20, 110));
    }

    @Test
    void createBoardUpdateBoardTest() {
        Minesweeper minesweeper = new Minesweeper();
        boolean[][] oldBoard = minesweeper.getBoard();
        minesweeper.createBoard(20, 20, 30);
        assertFalse(Arrays.equals(oldBoard, minesweeper.getBoard()));
    }

    @Test
    void getBoardWithoutSolutionTest() {
        Minesweeper minesweeper = new Minesweeper();
        boolean[][] board = minesweeper.getBoard();
        char[][] boardWithoutSolution = minesweeper.getBoardWithoutSolution();
        for (int i = 0; i < boardWithoutSolution.length; i++) {
            for (int j = 0; j < boardWithoutSolution[i].length; j++) {
                if (board[i][j]) {
                    assertEquals(boardWithoutSolution[i][j], '*');
                } else {
                    assertEquals(boardWithoutSolution[i][j], '.');
                }
            }
        }
    }

    @Test
    void getBoardWithSolutionTest() {
        Minesweeper minesweeper = new Minesweeper();
        boolean[][] board = minesweeper.getBoard();
        char[][] boardWithSolution = minesweeper.getBoardWithSolution();
        for (int i = 0; i < boardWithSolution.length; i++) {
            for (int j = 0; j < boardWithSolution[i].length; j++) {
                if (board[i][j]) {
                    assertEquals(boardWithSolution[i][j], '*');
                } else {
                    assertTrue(boardWithSolution[i][j] >= '1' && boardWithSolution[i][j] <= '8');
                }
            }
        }
    }

}