package com.epam.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LongestPlateauTest {

    @Test
    void getNumbersSetNumberTest() {
        LongestPlateau longestPlateau = new LongestPlateau();
        int[] numbers = new int[0];
        longestPlateau.setNumbers(numbers);
        assertArrayEquals(numbers, longestPlateau.getNumbers());
    }

    @Test
    void getLongestContiguousSequenceIfArrayOfNumbersIsNull() {
        LongestPlateau longestPlateau = new LongestPlateau();
        longestPlateau.setNumbers(null);
        assertThrows(IllegalArgumentException.class, longestPlateau::getLongestContiguousSequence);
    }

    @Test
    void getLongestContiguousSequenceWhenArrayOfNumberIsEmpty() {
        LongestPlateau longestPlateau = new LongestPlateau();
        longestPlateau.setNumbers(new int[0]);
        assertThrows(IllegalArgumentException.class, longestPlateau::getLongestContiguousSequence);
    }

    @ParameterizedTest
    @MethodSource("intArrayProvider")
    void getLongestContiguousSequenceTest(int[] numbers, int[] resultSequence) {
        LongestPlateau longestPlateau = new LongestPlateau();
        longestPlateau.setNumbers(numbers);
        Map<Integer, Integer> sequence = longestPlateau.getLongestContiguousSequence();
        int[] receivedSequence = sequence.values().stream()
                .mapToInt(Integer::intValue)
                .toArray();
        assertArrayEquals(receivedSequence, resultSequence);
    }

    private static Stream<Arguments> intArrayProvider() {
        return Stream.of(
                Arguments.of(new int[]{1}, new int[0]),
                Arguments.of(new int[]{1, 1, 1}, new int[0]),
                Arguments.of(new int[]{1, 0, 1}, new int[0]),
                Arguments.of(new int[]{0, 1, 0}, new int[]{0, 1, 0}),
                Arguments.of(new int[]{0, 23, 23, 0}, new int[]{0, 23, 23, 0}),
                Arguments.of(new int[]{5, 4, 34, 0, 23, 23, 0, 34}, new int[]{0, 23, 23, 0}),
                Arguments.of(new int[]{6, 6, 45, 5, 34, 34, 6, 7, 7, 7, 4}, new int[]{6, 7, 7, 7, 4}),
                Arguments.of(new int[]{1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1}, new int[]{0, 1, 1, 0})
        );
    }
}